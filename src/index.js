import "@fortawesome/fontawesome-free/js/fontawesome"
import "@fortawesome/fontawesome-free/js/solid"
import "@fortawesome/fontawesome-free/js/regular"
import "@fortawesome/fontawesome-free/js/brands"

import "./scss/main.scss"
import "./scss/grid.scss"

import logo from "./images/logo.png"
import ipadImg from "./images/ipad.png"

const ipadHome = document.querySelector("#ipad")
const logoHeader = document.querySelector("#logo")

ipadHome.src = ipadImg
logoHeader.src = logo
